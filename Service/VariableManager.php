<?php

namespace Modstore\PersistentVariableBundle\Service;

use Doctrine\ORM\EntityManager;
use Modstore\PersistentVariableBundle\Entity\Variable;

class VariableManager
{
    protected $em;
    
    public function __construct(EntityManager $entityManager) 
    {
        $this->em = $entityManager;
    }

    public function set($name, $value)
    {
        $variable = $this->em->getRepository('ModstorePersistentVariableBundle:Variable')->findOneBy(['name' => $name]);
        
        if (null === $variable) {
            $variable = new Variable();
            $this->em->persist($variable);
        }

        $variable->setName($name);
        $variable->setValue($value);
        $this->em->flush();
    }
    
    public function get($name, $default = null)
    {
        $variable = $this->em->getRepository('ModstorePersistentVariableBundle:Variable')->findOneByName($name);
        
        return (null === $variable) ? $default : $variable->getValue();
    }
}
