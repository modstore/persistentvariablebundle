<?php

namespace Modstore\PersistentVariableBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Modstore\PersistentVariableBundle\Entity\Variable;

class LoadVariableData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $variable = new Variable();
        $variable->setName('example');
        $variable->setValue('example value');

        $manager->persist($variable);
        $manager->flush();
    }
}