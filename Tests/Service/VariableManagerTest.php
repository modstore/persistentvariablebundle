<?php

namespace Modstore\PersistentVariableBundle\Tests\Service;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Modstore\PersistentVariableBundle\Service\VariableManager;

class VariableManagerTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    private $container;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->loadFixtures([
            'Modstore\PersistentVariableBundle\DataFixtures\ORM\LoadVariableData',
        ]);
    }

    public function variableGetDataProvider()
    {
        return [
            ['example', 'default value', 'example value'],
            ['not_set', 'default value', 'default value'],
            ['not_set', null, null],
        ];
    }

    /**
     * @dataProvider variableGetDataProvider
     *
     * @param $name
     * @param $default
     * @param $expectedValue
     */
    public function testVariableGet($name, $default, $expectedValue)
    {
        $variableManager = new VariableManager($this->em);

        $value = $variableManager->get($name, $default);

        $this->assertEquals($expectedValue, $value);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}
